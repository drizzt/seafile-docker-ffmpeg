# BUILD stage

# Get the base image
FROM seafileltd/seafile-mc:latest as builder

# Patches & build seahub
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install -y gettext make nodejs
COPY *.conf /tmp/conf/
COPY patches/frontend/*.patch /tmp/patches/
RUN set -x && \
    cd /opt/seafile/seafile-server-$SEAFILE_VERSION/seahub && \
    sed -i 's/python / python3 /g' Makefile && \
    cat /tmp/patches/*.patch | patch -p1 && \
    cd frontend && \
    npm install && \
    CI=false npm run build && \
    cd .. && \
    pip3 install -r requirements.txt && \
    export pyver=$(python3 -c "import sys; print('{}.{}'.format(*sys.version_info[:2]))") && \
    export PYTHONPATH="$PWD/../seafile/lib64/python$pyver/site-packages:$PWD/thirdpart" && \
    export CCNET_CONF_DIR=/tmp/conf SEAFILE_CONF_DIR=/tmp/conf && \
    make dist

# RUN stage
# Get the base image
FROM seafileltd/seafile-mc:latest

# We need ffmpeg-python for video thumbnails
RUN apt-get install -y ffmpeg
RUN pip3 install ffmpeg-python

# Patch seahub files
COPY patches/seahub/*.patch /tmp/patches/
RUN set -x && \
    cd /opt/seafile/seafile-server-$SEAFILE_VERSION/seahub && \
    cat /tmp/patches/*.patch | patch -p1 && \
    rm -rf /tmp/patches/

# Copy the patched frontend files
RUN rm -rf /opt/seafile/seafile-server-*/seahub/media/assets/
COPY --from=builder /opt/seafile/seafile-server-$SEAFILE_VERSION/seahub/media/assets /opt/seafile/seafile-server-$SEAFILE_VERSION/seahub/media/assets/
